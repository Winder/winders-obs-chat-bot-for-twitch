## Version 1.2.5

###### General

* Users, which doesn't have set their username color, do now get a random username color for their chat messages, which is constant during the session (instead of just having white as username color).

## Version 1.2.4

###### General

* Reworked error handling.
* * The error message is no longer disappearing while the script restarts itself, if the error could not be fixed.
* * The error box is now showing the top most error, instead of the error, which wrote as first into the error box.
* * If the top most error could be fixed, the error box shows the second top most error (this continues, until all errors have been fixed).

###### Bug Fixes

* Fixed a bug, that the script was restarting itself multiple times at once.

## Version 1.2.3

###### General

* If the chat bot can't get the Twitch user token, a button appears to provide the user an option to authorize the chat bot (because the most likely cause for not getting the Twitch user token is, that the chat bot has not been authorized by the user).

## Version 1.2.2

###### General

* Added new constant THEME.
* * With this constant, it is possible to select a certain theme for the stream chat messages.
* * Currently, there are three themes: None, Twitch-Dark and Twitch-Default.

## Version 1.2.1

###### General

* Added new constant HIGHLIGHT_NEW_FOLLOWERS.
* * This constant is a switch to turn on / off detecting and highlighting new followers.

###### Bug Fixes

* Fixed a bug, that empty message were send and displayed in stream chat.

## Version 1.2.0

###### General

* Improved performance and memory usage.
* Some minor improvements.

###### Twitch IRC

* Removed embedded Twitch stream chat and replaced it by an own stream chat.
* * This was necessary, due to the new security feature of embedded Twitch stream chats - according to [this announcement](https://discuss.dev.twitch.tv/t/twitch-embedded-player-updates-in-2020/23956) -, which DOES NOT support the file:// protocol.

## Version 1.1.0

###### General

* Twitch user ID is now fetched, too. Therefore, it isn't a necessary credential, anymore.

###### Twitch API

* Adapted changes to Twitch API, according to [this announcement](https://discuss.dev.twitch.tv/t/requiring-oauth-for-helix-twitch-api-endpoints/23916).

###### Bug Fixes

* In case of an error fetching the Twitch user token, the used iframe is now removed.

## Version 1.0.1

###### General

* Added example values.

###### Bug Fixes

* Fixed missing charset (meta tag).
* Fixed a bug, that MAX_TRIES_REQUEST + 1 requests were made (instead of MAX_TRIES_REQUESTS requests), before sendRequest() did abort with an error.
* Fixed some comments.

## Version 1.0.0

###### General

* This is the initial version.