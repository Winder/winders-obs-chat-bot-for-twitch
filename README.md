# Winder's OBS Chat-Bot for Twitch (Version 1.2.5)

A chat-bot for Twitch, which runs inside your OBS.

### Features

* Displays your Twitch chat inside your stream at any position and size you want.
  * The chat is displayed in real time. As soon as the message is displayed in your chat window, it is displayed in your stream (without any delays you set up in your settings, because the chat is part of your stream).
  * Chat messages vanish after some time.
* Writes a welcome message for new followers into your Twitch chat (which will be displayed in your stream, because the message is part of the chat, which is part of your stream).
  * There won't be any big welcome message, which goes through your whole stream window. Just a short message in the chat. In that way, new followes are highlighted in a subtle manner, without disturbing your stream.
* If, for any reason, an error occurres, an error message is displayed on top of the Twitch chat (moving the chat down, so that no chat message is hidden).
  * Unrelated features might still work, if one feature has an error.
  * It will try to restart itself to get rid of the error(s).
    * Only features with errors will restart. Features without errors will not.
    * If all features could be restarted, successfully, the error message disappears.

### Prerequisites

* [OBS](https://obsproject.com/de) (Because the chat-bot is just a file, containing only HTML, CSS and JavaScript, it might run in other broadcaster softwares, too.)
* [Twitch Account](https://www.twitch.tv/) (The chat-bot works only with Twitch; not with other streaming services.)
* [Twitch Developer Account](https://dev.twitch.tv/) (Because the chat-bot is an application, which runs on your local machine, you need to get own developer credentials.)

### Installation

Just download the file "twitch_chat.html", enter your credentials and the installation is finished. To use it, just select it in a local browser source in your OBS.

A more [detailed guide](https://gitlab.com/Winder/winders-obs-chat-bot-for-twitch/-/wikis/Installation) can be found in the project wiki.
